
$.fn.serializeObject = function()
{
    var o = {};
    var formId = $(this).attr('id');
    var a = this.serializeArray();
    $.each(a, function() {
        var prefixo = this.name.substr(0,3);
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
            if (prefixo == 'cod' || prefixo == 'son' || prefixo == 'sex' || prefixo == 'cbo') {
                o[this.name + '_label'] = $('#'+formId+'_'+this.name+' option:selected').text();
            }
        }
    });
    return o;
};