(function( $ ){

    var $this;
    var settings = {
        'noresultrow'		: true,
        'noresultrowtext'	: 'Nenhum registro encontrado!',
        'customDel'			: '',
        'customEdit'		: '',
        'customSeparator'	: ' &nbsp; '
    };

    var config = {
        cols : 0,
        rows : 0,
        inputNoResult : false
    };

    var methods = {
        init : function( options ) {
            $this = this;

            if ( !$this ) {
                $.error('Tabale não selecionada');
            }

            settings.customDel = '<button type="button" onclick="$(\'#' + $this.attr('id') + '\').tabela().del(this);">Excluir</button>';

            if ( options ) {
                $.extend( settings , options );
            }

            return methods;
        },
        add : function( data, cssClassTr, rowOld ) {
            if( typeof data != 'object' ) {
                $.error( 'Coluna(s) não informa(s)!' );
            }

            if( typeof cssClassTr == 'undefined' ) {
                cssClassTr = '';
            }

            if( typeof rowOld == 'undefined' ) {
                rowOld = null;
            }

            var html = '<tr';
            if ( cssClassTr != '' ) {
                html += ' class="' + cssClassTr + '"';
            }
            html += '>';

            var cols = 0;

            $.each(data, function( ){
                var colspan	= (this.colspan || 1);
                var acrow	= (this.acrow || false);
                var innerHtml= (this.html || '');
                var cssClassTd= (this.cssClassTd || '');

                html += '<td';
                if ( colspan > 1 ) {
                    html += ' colspan="' + colspan + '"';
                }
                if ( cssClassTd != '' ) {
                    html += ' class="' + cssClassTd + '"';
                }
                html += '>';
                html += innerHtml;
                html += '</td>';

                cols += colspan;
            });

            if (!config.inputNoResult){
                if ( methods.getNumCols() != cols) {
                    cols++;
                    html += '<td>';
                    html += settings.customDel;
                    if (settings.customEdit) {
                        html += settings.customSeparator;
                        html += settings.customEdit;
                    }
                    html += '</td>';
                }
            }
            html += '</tr>';

            config.inputNoResult = false;

            if ( methods.getNumCols() != cols) {
                $.error('Número de colunas inválido, passado ' + cols + ' e esperado ' + methods.getNumCols());
            }

            methods.delNoResultRow();
            if (rowOld) {
                $(rowOld).replaceWith(html);
            } else {
                $this.append(html);
            }

            return this;
        },
        del : function( row ) {

            if( typeof row != 'object' ) {
                $.error( 'Linha não informada' );
            }

            $(row).parent().parent().remove();

            methods.addNoResultRow();

            return this;
        },
        delNoResultRow : function ( ) {
            if (settings.noresultrow){
                var tds = $this.find("tbody tr:last td");
                if (tds && tds.length == 1 && tds.text() == 'Nenhum registro cadastrado'){
                    tds.parent().remove();
                }
            }
        },
        addNoResultRow : function ( ) {
            if (settings.noresultrow && methods.getNumRows() == 0){

                config.inputNoResult = true;

                var data = Array({
                    colspan	: methods.getNumCols(),
                    html	: settings.noresultrowtext,
                    acrow	: true
                });

                methods.add( data );
            }
        },
        getNumCols : function ( ) {
            return $this.find('tr:first').find('th,td').length || 0;
        },
        getNumRows : function ( ) {
            return $this.find('tbody tr').length || 0;
        }
    };

    $.fn.tabela = function( method ) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }
    };

})( jQuery );