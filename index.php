<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Artejato</title>
<link href="layout.css" rel="stylesheet" type="text/css">
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>

<div id="tudo">
<div id="conteudo">
<div id="topo">
 	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
      		<td align="left">
                <object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="200">
                  <param name="movie" value="topo.swf" />
                  <param name="quality" value="high" />
                  <param name="wmode" value="transparent" />
                  <param name="swfversion" value="6.0.65.0" />
                  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don't want users to see the prompt. -->
                  <param name="expressinstall" value="Scripts/expressInstall.swf" />
                  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                  <!--[if !IE]>-->
                  <object type="application/x-shockwave-flash" data="topo.swf" width="960" height="200">
                    <!--<![endif]-->
                    <param name="quality" value="high" />
                    <param name="wmode" value="transparent" />
                    <param name="swfversion" value="6.0.65.0" />
                    <param name="expressinstall" value="Scripts/expressInstall.swf" />
                    <!-- The browser displays the following alternative content for users with Flash Player 3.0 and older. -->
                    <div>
                      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
                      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
                    </div>
                    <!--[if !IE]>-->
                  </object>
                  <!--<![endif]-->
                </object>
		  </td>
   		</tr>
	</table> 
</div>

<!--<div id="contato" style="z-index:9999; float: left; position: relative; top: -85px; left: 700px;">
<a href="index.php?p=sup"><img src="img/suporte.png" border="0" /></a>
<img style="border-style: none;" src="http://messenger.services.live.com/users/ceda2c97589992e2@apps.messenger.live.com/presenceimage?mkt=pt-br" width="16" height="16" />
</div>//--><link href="layout.css" rel="stylesheet" type="text/css">
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>

<div id="principal">

<br />

<object id="FlashID" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="960" height="240">
  <param name="movie" value="animacao.swf" />
  <param name="quality" value="high" />
  <param name="wmode" value="transparent" />
  <param name="swfversion" value="6.0.65.0" />
  <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
  <param name="expressinstall" value="Scripts/expressInstall.swf" />
  <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
  <!--[if !IE]>-->
  <object type="application/x-shockwave-flash" data="animacao.swf" width="960" height="240">
    <!--<![endif]-->
    <param name="quality" value="high" />
    <param name="wmode" value="transparent" />
    <param name="swfversion" value="6.0.65.0" />
    <param name="expressinstall" value="Scripts/expressInstall.swf" />
    <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
    <div>
      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
    </div>
    <!--[if !IE]>-->
  </object>
  <!--<![endif]-->
</object>
<img src="imagens/div_fundo.jpg" />
<br />

<center>
<table width="95%" cellpadding="5" cellspacing="5" class="pTabela1">
   <tr>
      <td width="33%" align="justify" valign="top">
      <font class="pFont2">Bem-Vindo!</font><br /><br />
      <font class="pFont3">Fundada no ano de 1.992 na cidade de Dois Córregos, interior do estado de São Paulo, a ARTEJATO 
      inicialmente prestava serviço de jateamento abrasivo e pintura especial em estruturas metálicas diversas, tendo como principal cliente  
      as usinas açucareiras. Devido a oportunidade de se desenvolver no ramo de embalagens e dispositivos de movimentação, a empresa 
      foi se moldando ao mercado e as necessidades de nossos clientes.</font>
      <br />
      <div align="right" style="margin-top: 5px;"><a href="index.php?p=emp" target="_self"><img src="imagens/botao_mais.png" border="0" /></a></div>
      </td>
      <td width="33%" align="justify" valign="top">
      <font class="pFont2">Nossa Tecnologia</font><br /><br />
      <font class="pFont3">
      Com objetivo de atender nossos clientes com o m&aacute;ximo de qualidade e agilidade, nossa empresa investe constantemente
      em novas tecnologias, m&aacute;quinas, equipamentos e na qualifica&ccedil;&atilde;o de nossa equipe de profissionais.
      Contamos com m&aacute;quinas controladas por CNC, tecnologia esta que proporciona uma repetividade na produ&ccedil;&atilde;o. Temos em nosso departamento 
      de engenharia condição de desenvolver novos produtos em 3D e simular as situações de uso. No setor de pl&aacute;stico (vacuum forming) fabricamos nossos 
      pr&oacute;prios moldes, onde temos a disposi&ccedil;&atilde;o uma ferramentaria equipada com Centro de Usinagem de &uacute;ltima gera&ccedil;&atilde;o, assim proporcionamos aos nossos clientes agilidade e menores custos.
      </font>
      <br />
      </td>
      <td width="33%" align="justify" valign="top">
      <font class="pFont2">Nossos Produtos</font><br /><br />
      <font class="pFont3">
      <u>Metal</u>: desenvolvimento e fabricação de embalagens e dispositivos met&aacute;licos para transporte e movimenta&ccedil;&atilde;o em geral (racks, ca&ccedil;ambas,
       pallets, plataformas, caixas, etc...)
       <br />
       <br />
       <u>Pl&aacute;stico:</u> desenvolvimetno e fabrica&ccedil;&atilde;o de embalagem pl&aacute;stica utilizando o processo de Vacuum Forming (blister, pallets, 
       caixas, bangeijas, separadores, etc...) 
      </font>
      <br />
      <div align="right" style="margin-top: 5px;"><a href="#"><img src="imagens/botao_mais.png" border="0" /></a></div>
      </td>
   </tr>
</table>
</center>
</div>


<div id="rodape2">
<table width="100%" cellpadding="5" cellspacing="0" border="0">
   <tr>
      <td width="27%" bgcolor="#666666">
      <font face="verdana" style="font-size: 12px; color:#FFF;"><b>Artejato</b></font><br /> 
      <font face="verdana" style="font-size: 10px; color:#FFF;"> Indústria de Embalagem Metátilicas LTDA<br />
      Avenida Léo Guaraldo, 1020 <br />Dois Córregos - SP - CEP 17.300-000</font>
      </td>
      <td width="37%" align="center" valign="middle" bgcolor="#666666">&nbsp;</td>
      <td width="5%" align="right" valign="middle" bgcolor="#666666"><font face="verdana" style="font-size: 12px; color:#FFF;"><img src="imagens/contato.png" border="0" /></font></td>
      <td width="31%" bgcolor="#666666">
      <font face="verdana" style="font-size: 12px; color:#FFF;"> Contato<br />
      </font>
      <font face="verdana" style="font-size: 12px; font-weight:bold; color:#FFF">(14) 3652-9111</font></td>
   </tr>
</table>
</div>
<link href="layout.css" rel="stylesheet" type="text/css">


<br />
<div id="rodape">
<table class="tbrodape">
	<tr><td><a href="index.php"> Home</a></td>
    	<td><a href="index.php?p=emp"> Empresa</a></td>
        <td><a href="index.php?p=pro">Produtos</a></td>
        <td><a href="index.php?p=serv">Serviços</a></td>
        <td><a href="index.php?p=ven">Vendas</a></td>
        <td><a href="index.php?p=cont">Contato</a></td></tr>
</table>
<table class="tbrodape2">
	<tr>
	  <td>Copyright 2010 ARTEJATO - Desenvolvedor: SIMBOLUS S. I. (www.simbolus.com.br)</td></tr>
</table>
</div>

